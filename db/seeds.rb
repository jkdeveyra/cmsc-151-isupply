Profile.destroy_all
User.destroy_all

p = Profile.new
p.first_name = "iSupply"
p.last_name = "Admin"
p.save

user = User.new
user.email = "upvtc.isupply@gmail.com"
user.username = "admin"
user.password = "123456"
user.add_role :admin
user.add_role :spso_personnel
user.save

p.user = user

cos = ArticleCategory.create!(name: 'Common Office Supplies', code: 'COS')

acetate = Article.create!(name: 'Acetate', code: 1, article_category: cos)
Item.create!(article: acetate, code: 'A', specification: 'Gauge No. 3.0, transparent', unit: 'meter', price: '18.0')
Item.create!(article: acetate, code: 'B', specification: 'Gauge No. 4.0, transparent', unit: 'roll', price: '930.0')

alcohol = Article.create!(name: 'Alcohol', code: 2, article_category: cos)
Item.create!(article: alcohol, code: 'A', specification: 'ethyl, 70%, 500-mL', unit: 'bottle', price: '62.50')
Item.create!(article: alcohol, code: 'B', specification: 'isoprophyl, 70%, 250-mL', unit: 'bottle', price: '38.00')
Item.create!(article: alcohol, code: 'C', specification: 'isoprophyl, 70%, 500-mL', unit: 'bottle', price: '65.00')

battery = Article.create!(name: 'Battery', code: 3, article_category: cos)
Item.create!(article: battery, code: 'A', specification: 'size AA, heavy-duty, penlight', unit: 'pc', price: '4.00')
Item.create!(article: battery, code: 'B', specification: 'size AA, alkaline', unit: 'pc', price: '4.00')
Item.create!(article: battery, code: 'C', specification: 'lithium, 3-volts', unit: 'pc', price: '130')
Item.create!(article: battery, code: 'D', specification: 'size D, heavy duty', unit: 'pc', price: '40')
Item.create!(article: battery, code: 'E', specification: '9-volts, heavy duty', unit: 'pc', price: '160.')

carbon = Article.create!(name: 'Carbon Paper', code: 4, article_category: cos)
Item.create!(article: carbon, code: 'A', specification: 'ordinary, 210mm x 297mm (A4), 100\'s/box', unit: 'box', price: '120.0')
Item.create!(article: carbon, code: 'B', specification: '216mm x 330mm (8-1/2" x 13")', unit: 'box', price: '110.0')

cartolina = Article.create!(name: 'Cartolina', code: 5, article_category: cos)
Item.create!(article: cartolina, code: 'A', specification: 'black, 572mm x 724mm (22-1/2" x 28-1/2")', unit: 'box', price: '110.0')
Item.create!(article: cartolina, code: 'B', specification: 'blue, 572mm x 724mm (22-1/2" x 28-1/2")', unit: 'pc', price: '4.0')
Item.create!(article: cartolina, code: 'C', specification: 'green, 572mm x 724mm (22-1/2" x 28-1/2")', unit: 'pc', price: '4.0')

air = Article.create!(name: 'Air Freshener', code: 6, article_category: cos)
Item.create!(article: air, code: 'C', specification: '(for car), lemon scent', unit: 'can', price: '75.0')

ballpen = Article.create!(name: 'Ball Point Pen', code: 7, article_category: cos)
Item.create!(article: ballpen, code: 'A', specification: 'fine point, black', unit: 'pc', price: '4.0')
Item.create!(article: ballpen, code: 'B', specification: 'fine point, blue', unit: 'pc', price: '4.0')
Item.create!(article: ballpen, code: 'C', specification: 'fine point, red', unit: 'pc', price: '4.0')

chalk = Article.create!(name: 'Chalk', code: 8, article_category: cos)
Item.create!(article: chalk, code: 'A', specification: 'dustless (small box)', unit: 'box', price: '11.0')
Item.create!(article: chalk, code: 'B', specification: 'colored (small box)', unit: 'box', price: '12.5')
Item.create!(article: chalk, code: 'C', specification: 'yellow enamel (big box)', unit: 'box', price: '12.5')

cea = ArticleCategory.create!(name: 'Common Electrical/Plumbing Supplies/Accessories', code: 'CES')

ballast = Article.create!(name: 'Ballast', code: 1, article_category: cea)
Item.create!(article: ballast, code: 'A', specification: '20 watts, 220volts, normal power factor', unit: 'pc', price: '65.4')
Item.create!(article: ballast, code: 'B', specification: '40watts, 220volts, normal power factor', unit: 'pc', price: '63.4')

bulb = Article.create!(name: 'Bulb', code: 2, article_category: cea)
Item.create!(article: bulb, code: 'A', specification: 'incandescent, bell/round type. 100 watts, 230 volts', unit: 'pc', price: '63.9')
Item.create!(article: bulb, code: 'B', specification: 'incandescent, bell type. 50 watts', unit: 'pc', price: '25.50')
Item.create!(article: bulb, code: 'C', specification: 'spotlight, 150W, 220V', unit: 'pc', price: '198.0')

fuse = Article.create!(name: 'Fuse', code: 3, article_category: cea)
Item.create!(article: fuse, code: 'A', specification: '30-amperes, 220v', unit: 'pc', price: '9.90')
Item.create!(article: fuse, code: 'B', specification: '60-amperes, 220V', unit: 'pc', price: '5.50')

tube = Article.create!(name: 'Fluorescent Tube', code: 4, article_category: cea)
Item.create!(article: tube, code: 'A', specification: '20-watts', unit: 'pc', price: '65.10')
Item.create!(article: tube, code: 'B', specification: '40-watts', unit: 'pc', price: '69.30')

teflon = Article.create!(name: 'Teflon Tape', code: 5, article_category: cea)
Item.create!(article: teflon, code: 'A', specification: '3/4', unit: 'roll', price: '16.50')

starter = Article.create!(name: 'Starter', code: 6, article_category: cea)
Item.create!(article: starter, code: 'A', specification: '4-40W, for flourescent tube', unit: 'pc', price: '5.50')

of = ArticleCategory.create!(name: 'Office Forms', code: 'OF')
basic = Article.create!(name: 'Basic Papers', code: 1, article_category: of)
Item.create!(article: basic, code: 'A', specification: 'for Faculty/REPS', unit: 'ream', price: '475.0')
Item.create!(article: basic, code: 'B', specification: 'for Administrative Personnel', unit: 'ream', price: '475.0')

leavecards = Article.create!(name: 'Leave Cards', code: 2, article_category: of)
Item.create!(article: leavecards , code: 'A',  unit: 'ream', price: '475.0')

cfolder = Article.create!(name: 'Cumulative Folders', code: 3, article_category: of)
Item.create!(article: cfolder, code: 'A',  unit: 'booklet', price: '15.0')

stockcards = Article.create!(name: 'Stock Cards', code: 4, article_category: of)
Item.create!(article: stockcards , code: 'A', unit:'pc', price: '5.0')

checkbook= Article.create!(name: 'Check Book', code: 5, article_category: of)
Item.create!(article: checkbook, code: 'A', unit:'each', price: '0.0')







