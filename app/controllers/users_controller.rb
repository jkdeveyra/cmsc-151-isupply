class UsersController < ApplicationController

  load_and_authorize_resource
  before_filter :authenticate_user!

  # GET /users
  # GET /users.json
  def index
    @users = User.all
    @roles = Role.all

    respond_to do |format|
      format.html { redirect_to profiles_path }
      format.json { render json: @users }
    end
  end

  # GET /users/1
  # GET /users/1.json
  def show
    @user = User.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @user }
    end
  end

  # GET /users/new
  # GET /users/new.json
  def new
    @user = User.new
    @user.profile = Profile.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @user }
    end
  end

  # GET /users/1/edit
  def edit
    @user = User.find(params[:id])
  end

  # POST /users
  # POST /users.json
  def create
    @user = User.new(params[:user])

    @user.profile.save

    respond_to do |format|
      if @user.save
        format.html { redirect_to @user, notice: 'User was successfully created.' }
        format.json { render json: @user, status: :created, location: @user }
      else
        format.html { render action: "new" }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  def create_from_profile

    profile = Profile.find(params[:user][:profile_id])
    @user = User.new(params[:user])

    @user.profile = profile
    profile.save

    respond_to do |format|
      if @user.save
        format.html { redirect_to @user, notice: 'User was successfully created.' }
        format.json { render json: @user, status: :created, location: @user }
      else
        format.html { redirect_to profiles_path }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /users/1
  # PUT /users/1.json
  def update
    @user = User.find(params[:id])

    respond_to do |format|
      if @user.update_attributes(params[:user])
        format.html { redirect_to @user, notice: 'User was successfully updated.' }
        format.json { head :ok }
      else
        format.html { render action: "edit" }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  def remove_role
    @user = User.find(params[:id])

    can_remove = !@user.admin? || params[:role] != 'admin'

    if can_remove
      @user.remove_role params[:role]
      @user.save

      respond_to do |format|
        format.html { redirect_to users_path, notice: 'Successfully removed role.' }
        format.json { head :ok }
      end
    else
      respond_to do |format|
        format.html { redirect_to users_path, alert: 'Unable to remove role.' }
        format.json { render status: :unprocessable_entity }
      end
    end
  end

  def add_role
    @user = User.find(params[:id])

    @user.add_role params[:role]

    redirect_to profiles_path
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user = User.find(params[:id])

    if !@user.admin?
      @user.destroy
    end

    respond_to do |format|
      format.html { redirect_to users_url }
      format.json { head :ok }
    end
  end
end
