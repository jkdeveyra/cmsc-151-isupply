class AppsController < ApplicationController
  # GET /apps
  # GET /apps.json

  before_filter :item_variables, :exept => [:index]
  before_filter :office_categories, :exept => [:index]
  before_filter :init_app_master
  def init_app_master
    @app_master = AppMaster.find(params[:app_master_id])
  end

  def office_categories
    @office_categories = Office.categories
  end

  def item_variables
    @item_category = ArticleCategory.all
    @item_category.sort_by! {|c| [c.name, c.article.name,c.article.item.specification]}
    @item_hash = Item.hash_items
  end

  def index
    @apps = @app_master.apps

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @apps }
    end
  end

  # GET /apps/1
  # GET /apps/1.json
  def show
    @app = @app_master.apps.find(params[:id])
    @annual_listing = @app.annual_listing

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @app }
    end
  end

  # GET /apps/new
  # GET /apps/new.json
  def new
    @app = @app_master.apps.build
    @app.office_category = params[:office_category]
    @annual_listing = @app.annual_listing

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @app }
    end
  end

  # GET /apps/1/edit
  def edit
    @app = @app_master.apps.find(params[:id])
  end

  # POST /apps
  # POST /apps.json
  def create
    @app =  @app_master.apps.build(params[:app])
    @annual_listing = @app.annual_listing

    respond_to do |format|
      if @app.save
        format.html { redirect_to app_master_path @app_master, notice: 'App was successfully created.' }
        format.json { render json: @app, status: :created, location: @app }
      else
        format.html { render action: "new" }
        format.json { render json: @app.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /apps/1
  # PUT /apps/1.json
  def update
    @app = @app_master.apps.find(params[:id])

    respond_to do |format|
      if @app.update_attributes(params[:app])
        format.html { redirect_to @app, notice: 'App was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @app.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /apps/1
  # DELETE /apps/1.json
  def destroy
    @app = @app_master.apps.find(params[:id])
    @app.destroy

    respond_to do |format|
      format.html { redirect_to apps_url }
      format.json { head :no_content }
    end
  end
end
