class PurchaseRequestsController < ApplicationController
  # GET /purchase_requests
  # GET /purchase_requests.json
  
  def index
    @purchase_requests = PurchaseRequest.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @purchase_requests }
    end
  end

  # GET /purchase_requests/1
  # GET /purchase_requests/1.json
  def show
    @purchase_request = PurchaseRequest.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @purchase_request }
    end
  end

  # GET /purchase_requests/new
  # GET /purchase_requests/new.json
  def new
    @purchase_request = PurchaseRequest.new

    if params[:class]
      @purchase_request.set_params(params[:class], params[:object_id])
    else
    @purchase_request.pr_lines.build
    end

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @purchase_request }
    end
  end

  # GET /purchase_requests/1/edit
  def edit
    @purchase_request = PurchaseRequest.find(params[:id])
  end

  # POST /purchase_requests
  # POST /purchase_requests.json
  def create
    has_error = false

    params[:purchase_request][:pr_lines].each do|pr_line|
      begin
        item_id = Item.find(pr_line[1][:item][:id]).id
      rescue
        item_id = Item.hash_full_name[pr_line[1][:item][:id]]
      ensure
        pr_line[1][:item][:id] = item_id
        if(item_id==nil)
          has_error = true
          pr_line[1][:item] = nil
        end
      end
    end

    @purchase_request = PurchaseRequest.new(params[:purchase_request])

    respond_to do |format|
      if !has_error && @purchase_request.save
        format.html { redirect_to @purchase_request, notice: 'Purchase request was successfully created.' }
        format.json { render json: @purchase_request, status: :created, location: @purchase_request }
      else
        format.html { render action: "new" }
        format.json { render json: @purchase_request.errors, status: :unprocessable_entity}
      end
    end
  end

  # PUT /purchase_requests/1
  # PUT /purchase_requests/1.json
  def update
    @purchase_request = PurchaseRequest.find(params[:id])

    respond_to do |format|
      if @purchase_request.update_attributes(params[:purchase_request])
        format.html { redirect_to @purchase_request, notice: 'Purchase request was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @purchase_request.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /purchase_requests/1
  # DELETE /purchase_requests/1.json
  def destroy
    @purchase_request = PurchaseRequest.find(params[:id])
    @purchase_request.destroy

    respond_to do |format|
      format.html { redirect_to purchase_requests_url }
      format.json { head :no_content }
    end
  end
  
  def suggestions
    @hook = params[:hook]
    value = params[:value]
    @item_list = Item.array_typeahead
  end
end
