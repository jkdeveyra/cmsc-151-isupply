class RegistrationController < Devise::RegistrationsController

  # GET /signup
  def new
    @user = User.new
    @user.profile = Profile.new

    respond_with @user, location: new_user_registration_path, layout: "application-nocontainer"
  end

  # POST /signup
  def create

    @user = User.new(params[:user])

    if @user.valid? & @user.profile.valid?

      @user.save
      @user.profile.save

      puts "after saving entities"

      sign_in :user, @user

      redirect_to root_path
    else
      render action: "new", layout: "application-nocontainer"
    end
  end
end