class ApplicationController < ActionController::Base
  rescue_from CanCan::AccessDenied do |exception|
    redirect_to home_url, :alert => "You are not authorize to access that page"
  end
  protect_from_forgery
  
  before_filter :set_exist, :only => [:edit,:update]


  helper_method :super_admin?

  def super_admin?(profile = @profile)
    !profile.nil? && profile.first_name == "iSupply" && profile.last_name == "Admin"
  end
  
  def set_exist
  	@exists = true
  end
#  
#  def unset_exist
#  	@exists = true
#  end
end

