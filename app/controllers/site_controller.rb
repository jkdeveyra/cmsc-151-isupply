class SiteController < ApplicationController

  def index
    if signed_in?
      redirect_to home_path
    else
      redirect_to new_user_session_path
    end
  end

  def home

    if !signed_in?
      redirect_to new_user_session_path
    elsif current_user.has_role? :spso_personnel
      render 'spso_home'
    else
      @profile = current_user.profile
      render 'profiles/show'
    end
  end

  def about

    #add()

    #reindex()

    #search()
  end

  def reindex
    EquipmentSpec.all.each do |spec|
      spec.update_index
    end
  end

  def search
    puts "======================================================"
    EquipmentSpec.tire.search("canon").each do |spec|
      puts spec.specification
    end
    puts "======================================================"
  end

  def add
    specs = EquipmentSpec.new(specification: "Canon 1300", price: BigDecimal("35.00"))
    specs.equipment = Equipment.new(name: "Printer")
    specs.equipment.category = Category.new(name: "Printing Supplies")

    specs.equipment.category.save
    specs.equipment.save
    specs.save
  end
end
