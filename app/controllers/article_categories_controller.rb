class ArticleCategoriesController < ApplicationController

  load_and_authorize_resource
  before_filter :authenticate_user!

  # GET /article_categories
  # GET /article_categories.json
  def index

    #if (params[:term].present?)
    #  @article_categories = ArticleCategory.where(name: "/^" + params[:term] + "/")
    #else
      @article_categories = ArticleCategory.all
    #end

    #@id = ArticleCategory.first.id
    respond_to do |format|
      format.json { render json: @article_categories }
    end
  end

  # GET /article_categories/1
  # GET /article_categories/1.json
  def show
    @article_category = ArticleCategory.find(params[:id])

    respond_to do |format|
      format.json { render json: @article_category }
    end
  end

  # GET /article_categories/new
  # GET /article_categories/new.json
  def new
    @article_category = ArticleCategory.new
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @article_category }
    end
  end

  # GET /article_categories/1/edit
  def edit
    @article_category = ArticleCategory.find(params[:id])
  end

  # POST /article_categories
  # POST /article_categories.json
  def create
    @article_category = ArticleCategory.new(params[:article_category])

    respond_to do |format|
      if @article_category.save
        format.html { redirect_to Item, notice: 'Article category was successfully created.' }
        format.json { render json: @article_category, status: :created, location: @article_category }
      else
        format.html { render action: "new" }
        format.json { render json: @article_category.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /article_categories/1
  # PUT /article_categories/1.json
  def update
    @article_category = ArticleCategory.find(params[:id])

    respond_to do |format|
      if @article_category.update_attributes(params[:article_category])
        format.html { redirect_to edit_article_category_path, notice: 'Article category was successfully updated.' }
        format.json { head :ok }
      else
        format.html { render action: "edit" }
        format.json { render json: @article_category.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /article_categories/1
  # DELETE /article_categories/1.json
  def destroy
    @article_category = ArticleCategory.find(params[:id])
    @article_category.destroy

    respond_to do |format|
      format.html { redirect_to items_path }
      format.json { head :ok }
    end
  end
end
