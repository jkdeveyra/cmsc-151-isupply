class AppMastersController < ApplicationController
  # GET /app_masters
  # GET /app_masters.json
  before_filter :item_variables, :exept => [:index]
  before_filter :office_categories, :only => [:show]
  def item_variables
    @item_category = ArticleCategory.all
    @item_category.sort_by! {|c| [c.name, c.article.name,c.article.item.specification]}
    @item_hash = Item.hash_items
  end

  def office_categories
    @office_categories = Office.categories
  end

  def index
    @app_masters = AppMaster.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @app_masters }
    end
  end

  # GET /app_masters/1
  # GET /app_masters/1.json
  def show
    @app_master = AppMaster.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @app_master }
    end
  end

  # GET /app_masters/new
  # GET /app_masters/new.json
  def new
    @app_master = AppMaster.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @app_master }
    end
  end

  # GET /app_masters/1/edit
  def edit
    @app_master = AppMaster.find(params[:id])
  end

  # POST /app_masters
  # POST /app_masters.json
  def create
    @app_master = AppMaster.new(params[:app_master])
    update_lines_from_form

    respond_to do |format|
      if @app_master.save
        format.html { redirect_to @app_master, notice: 'App template was successfully created.' }
        format.json { render json: @app_master, status: :created, location: @app_master }
      else
        format.html { render action: "new" }
        format.json { render json: @app_master.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /app_masters/1
  # PUT /app_masters/1.json
  def update
    @app_master = AppMaster.find(params[:id])
    update_lines_from_form

    respond_to do |format|
      if @app_master.update_attributes(params[:app_master])
        format.html { redirect_to @app_master, notice: 'App template was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @app_master.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /app_masters/1
  # DELETE /app_masters/1.json
  def destroy
    @app_master = AppMaster.find(params[:id])
    @app_master.destroy

    respond_to do |format|
      format.html { redirect_to app_masters_url }
      format.json { head :no_content }
    end
  end

  def distribute
    @app_master = AppMaster.find(params[:id])
    @offices = Office.all
  end

  def deliver
    @app_master = AppMaster.find(params[:id])
    offices = Array.new

    params[:offices].each do |office|
      offices << Office.find(:first,:conditions=>{:name=>office})
    end

    @app_master.build_app_for offices

    respond_to do |format|
      format.html { redirect_to app_master_url(@app_master) }
      format.json { head :no_content }
    end

    flash[:notice] = "Successfully delivered APP"
  end

  def deliver_to_offices
    @app_master = AppMaster.find(params[:id])
    @app_master.update_price_lines

    @app_master.build_app_for Office.all

    respond_to do |format|
      format.html { redirect_to app_master_url(@app_master) }
      format.json { head :no_content }
    end

    flash[:notice] = "Successfully delivered APP"
  end

  private

  def update_lines_from_form
    @app_master.app_master_lines.delete_all
    if(params[:selected_items])
      params[:selected_items].each do|item|
        line = @app_master.app_master_lines.build
        line.set_item Item.find(item)
      end
    end
  end

end
