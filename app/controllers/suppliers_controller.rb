class SuppliersController < ApplicationController
  # GET /suppliers
  # GET /suppliers.json
  before_filter :phone_types, :exept => [:index, :delete_phone]
  before_filter :item_variables, :exept => [:index]
  
  def phone_types
  	@phone_types = Phone.types
  end
  
  def item_variables
  	@item_category = ArticleCategory.all
  	@item_category.sort_by! {|c| [c.name, c.article.name,c.article.item.specification]}
  	@item_hash = Item.hash_items
  end
  
  def index
    @suppliers = Supplier.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @suppliers }
    end
  end

  # GET /suppliers/1
  # GET /suppliers/1.json 
  def show
    @supplier = Supplier.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @supplier }
    end
  end

  # GET /suppliers/new
  # GET /suppliers/new.json
  def new
    @supplier = Supplier.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @supplier }
    end
  end

  # GET /suppliers/1/edit
  def edit
    @supplier = Supplier.find(params[:id])
  end

  # POST /suppliers
  # POST /suppliers.json
  def create
    @supplier = Supplier.new(params[:supplier])

    respond_to do |format|
      if @supplier.save
        format.html { redirect_to @supplier, notice: 'Supplier was successfully created.' }
        format.json { render json: @supplier, status: :created, location: @supplier }
      else
        format.html { render action: "new" }
        format.json { render json: @supplier.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /suppliers/1
  # PUT /suppliers/1.json
  def update
    @supplier = Supplier.find(params[:id])

    respond_to do |format|
      if @supplier.update_attributes(params[:supplier])
        format.html { redirect_to @supplier, notice: 'Supplier was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @supplier.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /suppliers/1
  # DELETE /suppliers/1.json
  def destroy
    @supplier = Supplier.find(params[:id])
    @supplier.destroy

    respond_to do |format|
      format.html { redirect_to suppliers_url }
      format.json { head :no_content }
    end
  end
  
  def add_contact
		supplier = Supplier.new
		contact = supplier.contacts.build
		@content = params[:content].gsub(/#{params[:contact_id].to_s}/,contact.id.to_s)

		respond_to do |format|
			format.html	
			format.js #{render '/phones/add_phone.js.erb'}
		end
  	end
  	
  def delete_contact
	  	unless params[:is_new].nil?
	  		supplier = Supplier.find(params[:id])
	  		contact = supplier.contacts.find(params[:contact_id])
	  		contact.destroy
	  	end
	  	
	  	@contact_tag = "#contact_#{params[:contact_id]}"
	  	respond_to do |format|
	  		format.html	
		    format.js
		end
  	end
  
 	
end
