class CollegesController < ApplicationController

  load_and_authorize_resource
  before_filter :authenticate_user!

  def index
    @colleges = College.all
  end

  def show
    @college = College.find(params[:id])
  end

  def new
    @college = College.new
  end

  def edit
    @college = College.find(params[:id])

  end

  def create
    @college = College.new(params[:college])

    if @college.save
      redirect_to @college, notice: "College was successfully created"
    else
      render action: 'new'
    end
  end

  def update
    @college = College.find(params[:id])

    if @college.update_attributes(params[:college])
      redirect_to @college, notice: "College was successfully updated"
    else
      render action: 'edit'
    end
  end

  def destroy
    @college = College.find(params[:id])
    @college.destroy

    redirect_to colleges_url, notice: "Successfully deleted college"
  end
end
