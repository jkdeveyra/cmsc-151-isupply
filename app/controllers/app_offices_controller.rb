class AppOfficesController < ApplicationController

  #  load_and_authorize_resource
  #    authorize! :manage, App

  before_filter :authenticate_user!
  before_filter :init_office
  before_filter :item_variables, :exept => [:index]
  def item_variables
    @item_category = ArticleCategory.all
    @item_category.sort_by! {|c| [c.name, c.article.name,c.article.item.specification]}
    @item_hash = Item.hash_items
  end

  def init_office
    @office = Office.find(params[:office_id])
  end

  def index
    @app_offices = @office.app_offices
  end

  def show
    @app_office = @office.app_offices.find(params[:id])
  end

  def edit
    @app_office = @office.app_offices.find(params[:id])
  end

  def print
    @app = @office.app_offices.find(params[:id])
  end

  def update
    @app_office =  @office.app_offices.find(params[:id])

    if @app_office.update_attributes(params[:app_office])
      redirect_to office_app_office_path @office, notice: "Successfully updated APP."
    else
      render action: 'edit'
    end
  end

  def destroy
    @app_office =  @office.app_offices.find(params[:id])
    @app_office.destroy

    redirect_to office_app_offices_url @office, notice: "Successfully destroyed APP."
  end

  def submit
    @app_office =  @office.app_offices.find(params[:id])
    @app_office.send_app
    @app_office.save

    flash[:notice] = "Successfully sent APP"
    redirect_to office_app_office_url @office, @app_office
  end

end
