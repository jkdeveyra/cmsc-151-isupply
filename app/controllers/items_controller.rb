class ItemsController < ApplicationController

#  load_and_authorize_resource
  before_filter :authenticate_user!

  # GET /items
  # GET /items.json
  def index

    #q = params[:q]
    #if !q.nil? and !q.empty?
    #  query = q + "*"
    #
    #  s = Tire.search 'items' do
    #    query do
    #      string "specification:#{query} OR article:#{query}"
    #    end
    #  end
    #
    #  @items = s.results.map do |doc|
    #    Item.find(doc.id)
    #  end
    #
    #  #
    #  #@items = Item.tire.search load: true do
    #  #  query do
    #  #    string "specification:#{query} OR article:#{query}"
    #  #  end
    #  #end
    #else
    @items = Item.all
    #end

    @categories = ArticleCategory.all

    @article_category = ArticleCategory.new

    @result = @items.group_by do |item|
      item.article
    end.group_by do |article, items|
      article.article_category
    end

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @result }
    end
  end

  # GET /items/1
  # GET /items/1.json
  def show

    if can? :edit, Item
      redirect_to action: :edit
    else
      @item = Item.find(params[:id])

      respond_to do |format|
        format.html # show.html.erb
        format.json { render json: @item }
      end
    end
  end

  # GET /items/new
  # GET /items/new.json
  def new
    @item = Item.new

    if !params[:aid].nil?
      @article = Article.find(params[:aid])

      if !@article.nil?
        @category = @article.article_category
        @item.article = @article
        @item.code = @article.next_item_code
      else
        @article = Article.new
        @category = ArticleCategory.new
      end
    elsif  !params[:cid].nil?
      @category = ArticleCategory.find(params[:cid])
      @category = ArticleCategory.new if @category.nil?
      @article = Article.new
    else
      @category = ArticleCategory.new
      @article = Article.new
    end

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @item }
    end
  end


  # GET /items/1/edit
  def edit
    @item = Item.find(params[:id])
    @article = @item.article
    @category = @article.article_category

    if @item.code.nil?
       @item.code = @article.next_item_code
    end
  end

  # POST /items
  # POST /items.json
  def create
    @item = Item.new(params[:item])

    @article = Article.find_or_initialize_by(name: params[:article][:name])
    @article.assign_attributes(params[:article])

    @category = ArticleCategory.find_or_initialize_by(name: params[:category][:name])
    @category.assign_attributes(params[:category])

    @article.article_category = @category
    @item.article = @article

    respond_to do |format|
      if @item.valid? & @article.valid? & @category.valid?

        @item.save

        format.html { redirect_to items_path }
        format.json { render json: @item, status: :created, location: @item }
      else
        format.html { render action: "new" }
        format.json { render json: @item.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /items/1
  # PUT /items/1.json
  def update
    @item = Item.find(params[:id])
    @item.assign_attributes(params[:item])

    @article = @item.article
    @article.assign_attributes(params[:article])

    @category = @article.article_category
    @category.assign_attributes(params[:category])

    respond_to do |format|
      if @item.valid? & @article.valid? & @category.valid?

        @item.save # Autosave is enable

        format.html { redirect_to items_path }
        format.json { head :ok }
      else
        format.html { render action: "edit" }
        format.json { render json: @item.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /items/1
  # DELETE /items/1.json
  def destroy
    @item = Item.find(params[:id])
    @item.destroy

    respond_to do |format|
      format.html { redirect_to items_url }
      format.json { head :ok }
    end
  end
end
