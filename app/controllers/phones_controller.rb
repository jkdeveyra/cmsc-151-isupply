class PhonesController < ApplicationController
	def delete
	  	unless params[:is_new].nil?
	  		ob_model = params[:object].classify.constantize
	  		object = ob_model.find(params[:id])
	  		phone = object.phones.find(params[:phone_id])
	  		phone.destroy
	  	end
	  	
	  	@phone_tag = "#phone_#{params[:phone_id]}"
	  	respond_to do |format|
	  		format.html	
		    format.js {render '/phones/delete_phone.js.erb'}
		end
  	end
  
 	def add
		ob_model = params[:object].classify.constantize
		object = ob_model.new
		phone = object.phones.build
		@content = params[:content].gsub(/#{params[:p_id].to_s}/,phone.id.to_s)

		respond_to do |format|
			format.html	
			format.js {render '/phones/add_phone.js.erb'}
		end
  	end
end
