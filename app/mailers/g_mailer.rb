class GMailer < ActionMailer::Base
  default from: "uptvc.isupply@gmail.com"

  def reset(user)
    @user = user

    mail to: @user.email,
         subject: "iSupply Password Reset",
         template_path: "devise/mailer",
         template_name: "reset_password_instructions"
  end
end
