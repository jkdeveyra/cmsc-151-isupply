module ItemsHelper
  def item_value(id)
    @item_hash[id]
  end

  def has_article(object, article)
    object.items.each do|item|
      if(item.article.name.eql? article.name)
      return true
      end
    end
    false
  end

  def has_category(object, category)
    object.items.each do|item|
      if(item.article.article_category.name.eql? category.name)
      return true
      end
    end
    false
  end

end
