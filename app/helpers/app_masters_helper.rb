module AppMastersHelper
  def sort_app_by_office(app_master)
    administrative = Array.new
    program = Array.new

    app_master.app_offices.each do|app_office|
      if(app_office.office.category==0)
      administrative << app_office
      else
      program << app_office
      end
    end

    program.each do|p|
      administrative << p
    end

    administrative
  end

  def distributed?(app_master = @app_master)
    app_master.app_offices.size != 0
  end

  def can_consolidate?(office_category)
    @app_master.app_offices.each do|app|
      if app.office.category == office_category && !app.sent
      return false
      end
    end
    true
  end

  def has_consolidated?(office_category)
    @app_master.apps.each do|app|
      if(app.office_category == office_category)
      return app
      end
    end
    nil
  end

end