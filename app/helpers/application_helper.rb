module ApplicationHelper



  def link_to_delete(item)
    link_to 'Destroy', item, confirm: "Are you sure?", method: :delete
  end

  def date_to_year(date)
    temp = "#{date}"
    temp = temp[0,4]
  end
  
  def tokenizer
		SecureRandom.urlsafe_base64
  end
  
  def to_name(object)
  	object.class.name.underscore
  end
 
end
