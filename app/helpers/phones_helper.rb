module PhonesHelper
	def phone_value(type)
		@phone_types[type].first	
	end
	
	def add_phone(object)
		new_phone = object.phones.build
		content = j render 'phones/fields' , :phone => new_phone, :exists => false,:object=>object
		link_to "Add contact number", add_phone_path(:id=>object.id,:p_id=>new_phone.id,:content=>content,:object=>object.class.name), :remote => true, :class=> "btn btn-mini"
	end
	
	def delete_phone(object,id,is_new)
		link_to "", delete_phone_path(:id=>object.id,:phone_id=>id,:is_new=>is_new ? is_new : nil,:object=>object.class.name),:remote =>true, confirm: "Are you sure?", :class => "icon-remove"
	end
end
