module AppOfficesHelper
  def stock_number_getter (item)
    article = item.article
    category = item.article_category
    stock_num = category.code
    stock_num.concat("-")
    stock_num.concat(article.code)
    stock_num.concat("-")
    stock_num.concat(item.stock_number)
  end

  def format_quantity(value)
    if value.nil? || value == 0
      '-'
    else
    value
    end
  end

  def format_amount(value)
    if value.nil? || value == 0
      '-'
    else
      number_with_precision value, precision: 2, delimiter: ','
    end
  end
end
