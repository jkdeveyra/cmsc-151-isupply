module SuppliersHelper
   def delete_contact(id,is_new)
      link_to "", delete_contact_supplier_path(:id=>@supplier.id,:contact_id=>id,:is_new=>is_new ? is_new : nil), :remote => true, confirm: "Are you sure?", :class=>"icon-remove"
   end
   
   def add_contact
      new_contact = @supplier.contacts.build
      content = j render "contacts/fields", :contact => new_contact, :exists => false
      link_to "Add contact person", add_contact_supplier_path(:id=>@supplier.id,:contact_id=>new_contact.id,:content=>content), :remote => true, :class =>"btn btn-mini"
   end
end
