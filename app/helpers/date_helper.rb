module DateHelper

  def date_absolute_format(date)

    today = Date.today

    if (date.year == today.year)
      if (date.mday == today.mday)
        s = date.strftime("%I:%M %p")
      else
        s = date.strftime("%B %d")
      end
    else
      s = date.strftime("%B %d, %Y")
    end

    "on " + s
  end

  def date_relative_format(date)

    time_today = Time.now
    time       = date.to_time

    diff_sec   = (time_today - time).round
    diff_min   = (diff_sec / 60).round
    diff_hour  = (diff_sec / 3600).round
    diff_day   = (diff_sec / 86400).round
    diff_month = (diff_sec / 2592000).round
    diff_year  = (diff_sec / 31104000).round

    if (diff_sec < 60)
      pluralize(diff_sec, "second") + " ago."
    elsif (diff_min < 60)
      pluralize(diff_min, "minute") + " ago."
    elsif (diff_hour < 24)
      pluralize(diff_hour, "hour") + " ago."
    elsif (diff_day < 30)
      pluralize(diff_day, "day") + " ago."
    elsif (diff_month < 12)
      pluralize(diff_month, "month") + " ago."
    else
      pluralize(diff_year, "year") + " ago."
    end
  end
end