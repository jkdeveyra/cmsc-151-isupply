module ProfilesHelper
  def role_link(user, role)
    if user.has_role? role
      user_path(user) + "/roles/remove/" + role.to_s
    else
      user_path(user) + "/roles/add/" + role.to_s
    end
  end

  def role_class(user, role)
    if user.has_role? role
      'role'
    else
      'not-role'
    end
  end
end
