class Item
  include Mongoid::Document

  resourcify

  attr_accessor :current_code

  field :code, type: String
  validates :code, presence: true

  field :stock_number, type: String
  validates :stock_number, presence: true

  field :specification, type: String, default: ''
  validates :specification, presence: true

  field :price, type: BigDecimal
  validates :price, presence: true

  field :unit, type: String
  validates :unit, presence: true

  belongs_to :article, autosave: true

  accepts_nested_attributes_for :article
  validates :article, presence: true

  delegate :article_category, to: :article

  validate :unique_item, on: :create
  validate :unique_item_code

  before_create :save_association, :tire_index

  has_and_belongs_to_many :suppliers, :dependent => :destroy

  embeds_many :item_prices

  def code=(c)
    write_attribute(:code, c.capitalize)
  end

  # Tire related
  #include Tire::Model::Search
  #include Tire::Model::Callbacks
  def tire_index

    #items = [self]
    #
    #Tire.index 'items' do
    #  import items
    #
    #  create
    #  items.each do |item|
    #    store id: item.id, specification: item.specification, article: item.article.name
    #  end
    #end
  end

  def update_price(price)
    self.item_prices.find_or_create_by_value(price)
  end

  def current_price
    if(self.item_prices.size==0)
      return price
    end
    self.item_prices.last
  end

  def to_indexed_json
    self.as_json
  end

  def increment_code

    article.incr_code

    if (article.code > article_category.last_code)
      article_category.last_code = article.code
    end
  end

  def self.hash_items
    items = all
    temp = items.map { |item| [item.id, "#{item.full_name}"] }
    Hash[*temp.flatten]
  end

  def self.hash_full_name
    items = all
    temp = items.map { |item| ["#{item.full_name}", item.id] }
    Hash[*temp.flatten]
  end

  def self.array_typeahead
    all.map { |item| item.full_name }
  end

  def full_name
    "#{self.article.name.upcase} #{self.specification}"
  end

  def stock_number
    if !article.nil?
      "#{article_category.code}-#{"%03d" % article.code}-#{code}"
    else
      ''
    end
  end

  private

  def save_association
    article_category.save
    article.save
  end

  def unique_item
    if Item.exists?(conditions: {specification: specification, unit: unit, article_id: article.id})
      errors.add(:id, "Item already exists.")
    end
  end

  def unique_item_code
    if Item.where(:_id.ne => id, article_id: article.id, code: code).exists?
      errors.add(:code, "exists on the same article")
    end
  end

  before_save :increment_code

end

class ItemPrice
  include Mongoid::Document
  include Mongoid::Timestamps::Created

  field :value, type: BigDecimal
  validates :value, presence: true, uniqueness: true

  embedded_in :item
end
