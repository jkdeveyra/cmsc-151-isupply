class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= User.new # guest user (not logged in)

    if user.has_role? :upvtc_employee
      can :read, AppOffice
      can :manage, User do |u|
        u == user
      end

      can :manage, Profile do |p|
        p == user.profile
      end

      cannot :manage, Role
    end

    if user.has_role? :spso_personnel
      can :manage, :all
      cannot :manage, User
    end

    if user.has_role? :admin
      can :manage, User
      can :manage, Profile
    end
  end
end
