class College
  include Mongoid::Document

  resourcify

  field :name, type: String
  validates :name, presence: true
end
