class AppMaster
  include Mongoid::Document
  include Mongoid::MultiParameterAttributes

  field :date_scheduled, :type => Date
  field :year, :type => Integer
  validates :year, numericality: {greater_than: 2000, less_than: 3000}

  accepts_nested_attributes_for :app_offices, :allow_destroy => true
  has_many :app_offices, :dependent => :destroy

  accepts_nested_attributes_for :apps, :allow_destroy => true
  has_many :apps, :dependent => :destroy

  embeds_many :app_master_lines
  accepts_nested_attributes_for :app_master_lines, :allow_destroy => true
  
  def build_app_for(offices)
    offices.each do |office|
      new_app = office.app_offices.build
      self.app_offices << new_app
      new_app.build_app_lines
      office.app_offices << new_app
      office.save!
    end
    self.save!
  end

  def apps_for_offices(office_category=nil)
    if office_category ==nil
    self.app_offices
    else
      self.app_offices.select {|app| app.office.category == office_category}
    end
  end

  def has_item?(item)
    self.app_master_lines.where(:item_id=>item.id).size!=0
  end

  def get_line(item)
    self.app_master_lines.where(:item_id=>item.id).first
  end

  def has_article_category?(article_category)
    self.app_master_lines.each do|line|
      if(line.item.article_category.eql? article_category)
      return true
      end
    end
    false
  end

  def has_article?(article)
    self.app_master_lines.each do|line|
      if(line.item.article.eql? article)
      return true
      end
    end
    false
  end
  
  def update_price_lines
    self.app_master_lines.each do|line|
      line.update_price
    end  
  end
end

class AppMasterLine
  include Mongoid::Document

  belongs_to :item

  field :price, type: BigDecimal

  embedded_in :app_master
  embedded_in :app_line
  embedded_in :pr_line
  
  def set_item(item)
    self.item = item
    update_price
  end

  def update_price
    self.price = self.item.current_price
  end
end
