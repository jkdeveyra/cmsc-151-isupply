class Supplier
  include Mongoid::Document
  
  field :name, type: String
  validates :name, presence: true, uniqueness: true
  
  field :tin, type: String
  validates :tin, uniqueness: true
  
  accepts_nested_attributes_for :phones, allow_destroy: true
  embeds_many :phones
   
  accepts_nested_attributes_for :contacts, allow_destroy: true
  embeds_many :contacts
  
  has_and_belongs_to_many :items
  
end

class Contact
  include Mongoid::Document
  
  field :name, type: String
  validates :name, presence: true, uniqueness: true
  
  embedded_in :supplier  
end
