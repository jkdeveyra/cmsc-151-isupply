class Office
  include Mongoid::Document

  field :name, type: String
  validates :name, presence: true, length: {minimum: 2}
  
  field :category, type: Integer
  validates :category, presence: true

  accepts_nested_attributes_for :phones, allow_destroy: true
  embeds_many :phones

#  belongs_to :college
  accepts_nested_attributes_for :app_offices, allow_destroy: true
  has_many :app_offices, :dependent => :destroy
  
  has_many :purchase_requests

  def reject(attr)
  	attr[:number].blank? || attr[:type].blank?
  end
  
  def self.categories
    arr = %w{Administrative Program}
    arr.each_with_index.map {|index,value| [index,value]}
  end
end
