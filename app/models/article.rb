class Article
  include Mongoid::Document

  resourcify

  field :next_item_code, type: String, default: 'A'

  field :name, type: String
  validates :name, presence: true, uniqueness: true

  field :code, type: Integer
  validates :code, presence: true, numericality: {only_integer: true, greater_than: 0}

  belongs_to :article_category, autosave: true
  accepts_nested_attributes_for :article_category
  validates :article_category, presence: true

  has_many :items, dependent: :destroy

  #include Tire::Model::Search
  #include Tire::Model::Callbacks

  def to_indexed_json
    self.as_json
  end

  def incr_code
    self.next_item_code = next_item_code.next
  end
end
