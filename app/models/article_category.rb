class ArticleCategory
  include Mongoid::Document

  resourcify

  field :name, type: String
  validates :name, presence: true, uniqueness: true

  field :code, type: String
  validates :code, presence: true, uniqueness: true

  field :last_code, type: Integer, default: 0

  has_many :articles, dependent: :destroy

  #include Tire::Model::Search
  #include Tire::Model::Callbacks

  def to_indexed_json
    self.as_json
  end
end
