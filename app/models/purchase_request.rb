class PurchaseRequest
  include Mongoid::Document
  include Mongoid::MultiParameterAttributes

  field :pr_no, :type => String
  field :pr_date, :type => Date
  field :sai_no, :type => String
  field :sai_date, :type => Date
  field :alobs_no, :type => String
  field :alobs_date, :type => Date
  field :purpose, :type => String

  embeds_many :pr_lines
  accepts_nested_attributes_for :pr_lines

  belongs_to :office
  belongs_to :app
  def set_params(class_s, id)
    object = class_s.classify.constantize
    instance = object.find(id)

    if object.equal? App
      self.app = instance
      self.purpose = "Annual Procurement Plan"
      quantities = instance.quantity_per_item
      instance.app_master.app_master_lines.each_with_index do|line, index|
        self.pr_lines << PrLine.new(:item => line.item, :price => line.price, :quantity=>quantities[index])
      end
    end

  end
end

class PrLine
  include Mongoid::Document

  belongs_to :item

  field :quantity, type: Integer
  field :price, type: BigDecimal

  embedded_in :purchase_request
end
