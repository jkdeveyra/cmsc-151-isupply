class Phone
  include Mongoid::Document
  
  field :type, type: Integer
  validates :type, presence: true

  field :number, type: String
  validates :number, presence: true, length: {in: 7..20}, uniqueness: true

  embedded_in :company
  embedded_in :offices
  embedded_in :supplier
  
  def self.types
  	arr = %w{Telephone Mobile Fax}
  	arr.each_with_index.map {|index,value| [index,value]}
  end
end
