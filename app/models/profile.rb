class Profile
  include Mongoid::Document

  resourcify

  field :first_name,  type: String
  validates :first_name, presence: true, length: {in: 2..30}

  field :middle_name, type: String

  field :last_name,   type: String
  validates :last_name, presence: true, length: {in: 2..30}

  field :deleted, type: Boolean, default: false

  embeds_many :employment_histories
  accepts_nested_attributes_for :employment_histories, allow_destroy: true

  has_one :user,  dependent: :destroy

  def full_name
    "#{first_name} #{middle_name.to_s} #{last_name}"
  end

  def has_account?
    !user.nil?
  end
end

class EmploymentHistory
  include Mongoid::Document
  include Mongoid::MultiParameterAttributes

  field :position, type: String
  validates :position,presence: true, length: {in: 2..30}

  field :from, type: Date
  validates :from, :presence => true

  field :to, type: Date
  validates :from, :presence => true

  belongs_to :office
  embedded_in :profile
end

