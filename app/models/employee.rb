class Employee
  include Mongoid::Document

  resourcify

  field :first_name, type: String
  validates :first_name, presence: true

  field :middle_name, type: String
  validates :middle_name, presence: true

  field :last_name, type: String
  validates :last_name, presence: true
  
end
