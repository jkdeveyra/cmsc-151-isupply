class AppOffice
  include Mongoid::Document

  resourcify

  field :sent, :type => Boolean
  field :date_sent, :type => DateTime
  field :control_number, :type => Integer

  accepts_nested_attributes_for :app_lines
  embeds_many :app_lines

  has_one :app_master, :inverse_of => :app_office
  validates :app_master, presence: true

  belongs_to :office
  belongs_to :app_master

  def build_app_lines
    app_master.app_master_lines.each do |line|
      new_line = app_lines.build
      new_line.app_master_line = line
      app_lines << new_line
    end
  end

  def send_app
    self.sent = true
    time = Time.now.to_s
    self.date_sent = DateTime.parse(time).strftime("%d/%m/%Y %H:%M:%S")
  end

  def regular
    t = app_lines.map { |l| l.total }
    t.inject(:+)
  end

  def contingency
    regular * 0.10
  end

  def total
    regular * 1.10
  end

  def app_line_by_category

    app_lines.group_by do |line |
      line.item.article.article_category
    end
  end
end

class AppLine
  include Mongoid::Document

  resourcify
  
  field :quarter1, type: Integer
  #  validates :quarter1, numericality: {greater_than_or_equal_to: 0}

  field :quarter2, type: Integer
  #  validates :quarter2,  numericality: {greater_than_or_equal_to: 0}

  field :quarter3, type: Integer
  #  validates :quarter3, numericality: {greater_than_or_equal_to: 0}

  field :quarter4, type: Integer
  #  validates :quarter4, numericality: {greater_than_or_equal_to: 0}

  embedded_in :app_office
  
  embeds_one :app_master_line
  
  def item
    app_master_line.item
  end

  def price
    app_master_line.price
  end
  
  def quarter1_amount

    if quarter1.nil?
      nil
    else
      quarter1 * price
    end
  end

  def quarter2_amount
    if quarter2.nil?
      nil
    else
      quarter2 * price
    end
  end

  def quarter3_amount
    if quarter3.nil?
      nil
    else
      quarter3 * price
    end
  end

  def quarter4_amount
    if quarter4.nil?
      nil
    else
      quarter4 * price
    end
  end

  def quantity
    count = 0;
    count += self.quarter1 if self.quarter1
    count += self.quarter2 if self.quarter2
    count += self.quarter3 if self.quarter3
    count += self.quarter4 if self.quarter4
    count
  end

  def total
    price * quantity
  end
end
