class User
  include Mongoid::Document
  extend Rolify

  rolify

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :authentication_keys => [:login]

  attr_accessible :login, :email, :username, :password, :password_confirmation, :profile_attributes
  attr_accessor :login

  ## Database authenticatable
  field :email, type: String, default: ""
  field :encrypted_password, type: String, default: ""
  field :username, type: String
  validates :username, presence: true, uniqueness: true

  validates :email, uniqueness: true

  ## Recoverable
  field :reset_password_token, :type => String
  field :reset_password_sent_at, :type => Time

  ## Rememberable
  field :remember_created_at, :type => Time

  ## Trackable
  field :sign_in_count, type: Integer, default: 0
  field :current_sign_in_at, type: Time
  field :last_sign_in_at, type: Time
  field :current_sign_in_ip, type: String
  field :last_sign_in_ip, type: String

  ## Confirmable
  #field :confirmed_at, type: Time
  #field :confirmation_token, type: String
  #field :confirmation_sent_at, type: Time
  #field :unconfirmed_email, type: Boolean

  belongs_to :profile
  validates :profile, presence: true
  accepts_nested_attributes_for :profile

  ## Token authenticatable
  # field :authentication_token, :type => String

  def self.find_first_by_auth_conditions(warden_conditions)
    conditions = warden_conditions.dup
    if login = conditions.delete(:login)
      self.any_of({:username => /^#{Regexp.escape(login)}$/i}, {:email => /^#{Regexp.escape(login)}$/i}).first
    else
      super
    end
  end

  def admin?
    username == 'admin'
  end
end
