class App
  include Mongoid::Document
  field :control_number, :type => String
  validates :control_number, presence: true

  field :office_category, :type => Integer
  validates :office_category, presence: true

  belongs_to :app_master
  
  has_many :purchase_requests
  accepts_nested_attributes_for :purchase_requests
  
  def app_offices
    self.app_master.app_offices.select {|app| app.office.category == self.office_category}
  end

  def annual_listing
    list = Array.new

    app_offices.each do|app_office|
      line = Array.new
      app_office.app_lines.each do|app_line|
        line << app_line.quantity
      end
      list << line
    end
    list
  end

  def quantity_per_item
    list = annual_listing
    quantities = Array.new
    
    list.each_with_index do|sub_list,outer_index|
      sub_list.each_with_index do|q,index|
        if(outer_index == 0)
          quantities[index] = q
        else
          quantities[index] += q
        end
      end    
    end
    quantities
  end
end
