# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/
$ ->

  $('#profile-form').submit (e) ->

    methodInput = $('input[name=_method')
    method = $(this).attr 'method'

    if methodInput.length
      method = methodInput.val()

    alert method

    $alert = $(this).find 'div.form-error'
    url = $(this).attr('action')

    firstName = $('#profile_first_name').val()
    middleName = $('#profile_middle_name').val()
    lastName = $('#profile_last_name').val()

    settings =
      data:
        'profile[first_name]': firstName
        'profile[middle_name]': middleName
        'profile[last_name]': lastName
      type: 'PUT'
      success: ->
        location.reload()

      error: (r) ->
        json = $.parseJSON(r.responseText)
        $alert.empty()

        for key, value of json
          $alert.append "<p>#{key} #{value}</p>"

        $alert.show()

    $.ajax url + '.json', settings
    false