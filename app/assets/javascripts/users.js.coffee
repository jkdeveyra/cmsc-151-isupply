# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/
$ ->

  $('#edit-user').submit (e) ->

    $alert = $(this).find 'div.form-error'
    url = $(this).attr('action')

    email = $('#user_email').val()
    password = $('#user_password').val()
    confirm = $('#user_password_confirmation').val()
    current = $('#user_current_password').val()

    settings =
      data:
        'user[email]': email
        'user[password]': password
        'user[password_confirmation]': confirm
        'user[current_password]': current
      type: 'PUT'
      success: ->
        location.reload()

      error: (r) ->
        json = $.parseJSON(r.responseText)
        $alert.empty()

        for key, value of json.errors
          $alert.append "<p>#{key} #{value}</p>"

        $alert.show()

    $.ajax url + '.json', settings
    false