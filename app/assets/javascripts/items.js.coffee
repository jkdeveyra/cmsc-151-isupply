$ ->
  $('div.category-items').mouseover ->
    $(this).find('a.add-item-cat').show()

  $('div.category-items').mouseout ->
    $(this).find('a.add-item-cat').hide()

  $('nav.category ul > li.cat').mouseover ->
    next = $(this).next()
    next.show()
    $(this).siblings('.add-item').not(next).hide()

  $('div.items').mouseover ->
    $('nav.category ul > li.add-item').hide()

  $('div.category-items ul > li.item').mouseover ->
    next = $(this).next()
    next.show()

    $('div.category-items ul > li.add-item').not(next).hide()