$ ->
  $('a.role').click ->
    $this = $(this)
    url = $this.attr 'href'

    settings =
      type: 'POST'
      complete: (r) ->
        if r.status == 200
          if url.indexOf('add') >= 0
            $this.parent().attr 'class', 'role'
            $this.attr 'href', url.replace /add/, 'remove'

          else
            $this.parent().attr 'cl\ass', 'not-role'
            $this.attr 'href', url.replace /remove/, 'add'

    $.ajax url + '.json', settings
    false

  $('#create-account-form').submit ->
    $alert = $(this).find 'div.form-error'
    url = $(this).attr('action')

    setings =
      data:
        'user[profile_id]': $('#profile_id').val()
        'user[username]': $('#user_username').val()
        'user[email]': $('#user_email').val()
        'user[password]': $('#user_password').val()
        'user[password_confirmation]': $('#user_password_confirmation').val()
      type: 'POST'
      success: (r) ->
        location.reload()

      error: (r) ->
        json = $.parseJSON(r.responseText)

        $alert.empty()

        for key, value of json
          $alert.append "<p>#{key} #{value}</p>"

        $alert.show()

    $.ajax url + '.json', setings
    false

  $('ul.profiles > li').mouseover ->
    $(this).find('div.close').show()

  $('ul.profiles > li').mouseout ->
    $(this).find('div.close').hide()

  $("a.toggle-profile-form").click ->
    $profileForm = $(".profile-form")

    if $profileForm.is ':visible'
      $(this).html '<i class="icon-plus"></i> Create Profile'

    else
      $(this).html "Hide"
      $profileForm.find('input[type=text]').val ''

    $profileForm.toggle()

  $(".item-last-login").mouseover ->
    $(".relative-date").hide()
    $(".absolute-date").show()

  $(".item-last-login").mouseout ->
    $(".relative-date").show()
    $(".absolute-date").hide()