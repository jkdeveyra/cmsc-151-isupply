$(function() {
	$('#item_field').keyup(function() {
		var value = $(this).val()
		if (value.length == 3) {
			$.ajax({
				url: $(this).data('url'),
				data: {
					hook: $(this).data('hook'),
					value: value
				}
			})
		}
	})
})

