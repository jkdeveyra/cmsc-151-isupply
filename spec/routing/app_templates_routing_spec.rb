require "spec_helper"

describe AppTemplatesController do
  describe "routing" do

    it "routes to #index" do
      get("/app_templates").should route_to("app_templates#index")
    end

    it "routes to #new" do
      get("/app_templates/new").should route_to("app_templates#new")
    end

    it "routes to #show" do
      get("/app_templates/1").should route_to("app_templates#show", :id => "1")
    end

    it "routes to #edit" do
      get("/app_templates/1/edit").should route_to("app_templates#edit", :id => "1")
    end

    it "routes to #create" do
      post("/app_templates").should route_to("app_templates#create")
    end

    it "routes to #update" do
      put("/app_templates/1").should route_to("app_templates#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/app_templates/1").should route_to("app_templates#destroy", :id => "1")
    end

  end
end
