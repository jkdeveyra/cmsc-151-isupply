require "spec_helper"

describe PurchaseRequestsController do
  describe "routing" do

    it "routes to #index" do
      get("/purchase_requests").should route_to("purchase_requests#index")
    end

    it "routes to #new" do
      get("/purchase_requests/new").should route_to("purchase_requests#new")
    end

    it "routes to #show" do
      get("/purchase_requests/1").should route_to("purchase_requests#show", :id => "1")
    end

    it "routes to #edit" do
      get("/purchase_requests/1/edit").should route_to("purchase_requests#edit", :id => "1")
    end

    it "routes to #create" do
      post("/purchase_requests").should route_to("purchase_requests#create")
    end

    it "routes to #update" do
      put("/purchase_requests/1").should route_to("purchase_requests#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/purchase_requests/1").should route_to("purchase_requests#destroy", :id => "1")
    end

  end
end
