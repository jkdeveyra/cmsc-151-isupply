require 'spec_helper'

describe "colleges/index.html.erb" do
  before(:each) do
    assign(:colleges, [
      stub_model(College,
        :name => "Name"
      ),
      stub_model(College,
        :name => "Name"
      )
    ])
  end

  it "renders a list of colleges" do
    render
    # Run the generator again with the --webrat-matchers flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Name".to_s, :count => 2
  end
end
