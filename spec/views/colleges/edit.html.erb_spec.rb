require 'spec_helper'

describe "colleges/edit.html.erb" do
  before(:each) do
    @college = assign(:college, stub_model(College,
      :new_record? => false,
      :name => "MyString"
    ))
  end

  it "renders the edit college form" do
    render

    # Run the generator again with the --webrat-matchers flag if you want to use webrat matchers
    assert_select "form", :action => college_path(@college), :method => "post" do
      assert_select "input#college_name", :name => "college[name]"
    end
  end
end
