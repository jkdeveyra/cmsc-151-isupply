require 'spec_helper'

describe "colleges/show.html.erb" do
  before(:each) do
    @college = assign(:college, stub_model(College,
      :name => "Name"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat-matchers flag if you want to use webrat matchers
    rendered.should match(/Name/)
  end
end
