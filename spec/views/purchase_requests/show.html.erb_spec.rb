require 'spec_helper'

describe "purchase_requests/show" do
  before(:each) do
    @purchase_request = assign(:purchase_request, stub_model(PurchaseRequest,
      :pr_no => "",
      :pr_date => "",
      :sai_no => "",
      :sai_date => "",
      :alobs_no => "",
      :alobs_date => "",
      :purpose => ""
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(//)
    rendered.should match(//)
    rendered.should match(//)
    rendered.should match(//)
    rendered.should match(//)
    rendered.should match(//)
    rendered.should match(//)
  end
end
