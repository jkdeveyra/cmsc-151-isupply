require 'spec_helper'

describe "purchase_requests/index" do
  before(:each) do
    assign(:purchase_requests, [
      stub_model(PurchaseRequest,
        :pr_no => "",
        :pr_date => "",
        :sai_no => "",
        :sai_date => "",
        :alobs_no => "",
        :alobs_date => "",
        :purpose => ""
      ),
      stub_model(PurchaseRequest,
        :pr_no => "",
        :pr_date => "",
        :sai_no => "",
        :sai_date => "",
        :alobs_no => "",
        :alobs_date => "",
        :purpose => ""
      )
    ])
  end

  it "renders a list of purchase_requests" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "".to_s, :count => 2
    assert_select "tr>td", :text => "".to_s, :count => 2
    assert_select "tr>td", :text => "".to_s, :count => 2
    assert_select "tr>td", :text => "".to_s, :count => 2
    assert_select "tr>td", :text => "".to_s, :count => 2
    assert_select "tr>td", :text => "".to_s, :count => 2
    assert_select "tr>td", :text => "".to_s, :count => 2
  end
end
