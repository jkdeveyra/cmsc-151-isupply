require 'spec_helper'

describe "purchase_requests/new" do
  before(:each) do
    assign(:purchase_request, stub_model(PurchaseRequest,
      :pr_no => "",
      :pr_date => "",
      :sai_no => "",
      :sai_date => "",
      :alobs_no => "",
      :alobs_date => "",
      :purpose => ""
    ).as_new_record)
  end

  it "renders new purchase_request form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => purchase_requests_path, :method => "post" do
      assert_select "input#purchase_request_pr_no", :name => "purchase_request[pr_no]"
      assert_select "input#purchase_request_pr_date", :name => "purchase_request[pr_date]"
      assert_select "input#purchase_request_sai_no", :name => "purchase_request[sai_no]"
      assert_select "input#purchase_request_sai_date", :name => "purchase_request[sai_date]"
      assert_select "input#purchase_request_alobs_no", :name => "purchase_request[alobs_no]"
      assert_select "input#purchase_request_alobs_date", :name => "purchase_request[alobs_date]"
      assert_select "input#purchase_request_purpose", :name => "purchase_request[purpose]"
    end
  end
end
