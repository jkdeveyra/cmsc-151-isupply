require 'spec_helper'

describe "purchase_requests/edit" do
  before(:each) do
    @purchase_request = assign(:purchase_request, stub_model(PurchaseRequest,
      :pr_no => "",
      :pr_date => "",
      :sai_no => "",
      :sai_date => "",
      :alobs_no => "",
      :alobs_date => "",
      :purpose => ""
    ))
  end

  it "renders the edit purchase_request form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => purchase_requests_path(@purchase_request), :method => "post" do
      assert_select "input#purchase_request_pr_no", :name => "purchase_request[pr_no]"
      assert_select "input#purchase_request_pr_date", :name => "purchase_request[pr_date]"
      assert_select "input#purchase_request_sai_no", :name => "purchase_request[sai_no]"
      assert_select "input#purchase_request_sai_date", :name => "purchase_request[sai_date]"
      assert_select "input#purchase_request_alobs_no", :name => "purchase_request[alobs_no]"
      assert_select "input#purchase_request_alobs_date", :name => "purchase_request[alobs_date]"
      assert_select "input#purchase_request_purpose", :name => "purchase_request[purpose]"
    end
  end
end
