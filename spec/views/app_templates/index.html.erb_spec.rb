require 'spec_helper'

describe "app_templates/index" do
  before(:each) do
    assign(:app_templates, [
      stub_model(AppTemplate,
        :date_scheduled => "Date Scheduled",
        :date => "Date",
        :year => "Year",
        :date => "Date",
        :control_number => "Control Number",
        :String => "String"
      ),
      stub_model(AppTemplate,
        :date_scheduled => "Date Scheduled",
        :date => "Date",
        :year => "Year",
        :date => "Date",
        :control_number => "Control Number",
        :String => "String"
      )
    ])
  end

  it "renders a list of app_templates" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Date Scheduled".to_s, :count => 2
    assert_select "tr>td", :text => "Date".to_s, :count => 2
    assert_select "tr>td", :text => "Year".to_s, :count => 2
    assert_select "tr>td", :text => "Date".to_s, :count => 2
    assert_select "tr>td", :text => "Control Number".to_s, :count => 2
    assert_select "tr>td", :text => "String".to_s, :count => 2
  end
end
