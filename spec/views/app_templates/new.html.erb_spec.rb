require 'spec_helper'

describe "app_templates/new" do
  before(:each) do
    assign(:app_template, stub_model(AppTemplate,
      :date_scheduled => "MyString",
      :date => "MyString",
      :year => "MyString",
      :date => "MyString",
      :control_number => "MyString",
      :String => "MyString"
    ).as_new_record)
  end

  it "renders new app_template form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => app_templates_path, :method => "post" do
      assert_select "input#app_template_date_scheduled", :name => "app_template[date_scheduled]"
      assert_select "input#app_template_date", :name => "app_template[date]"
      assert_select "input#app_template_year", :name => "app_template[year]"
      assert_select "input#app_template_date", :name => "app_template[date]"
      assert_select "input#app_template_control_number", :name => "app_template[control_number]"
      assert_select "input#app_template_String", :name => "app_template[String]"
    end
  end
end
