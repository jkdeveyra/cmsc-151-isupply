require 'spec_helper'

describe "app_templates/show" do
  before(:each) do
    @app_template = assign(:app_template, stub_model(AppTemplate,
      :date_scheduled => "Date Scheduled",
      :date => "Date",
      :year => "Year",
      :date => "Date",
      :control_number => "Control Number",
      :String => "String"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Date Scheduled/)
    rendered.should match(/Date/)
    rendered.should match(/Year/)
    rendered.should match(/Date/)
    rendered.should match(/Control Number/)
    rendered.should match(/String/)
  end
end
