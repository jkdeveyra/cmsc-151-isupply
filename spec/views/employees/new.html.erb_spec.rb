require 'spec_helper'

describe "employees/new.html.erb" do
  before(:each) do
    assign(:employee, stub_model(Employee,
      :first_name => "MyString",
      :middle_name => "MyString",
      :last_name => "MyString"
    ).as_new_record)
  end

  it "renders new employee form" do
    render

    # Run the generator again with the --webrat-matchers flag if you want to use webrat matchers
    assert_select "form", :action => employees_path, :method => "post" do
      assert_select "input#employee_first_name", :name => "employee[first_name]"
      assert_select "input#employee_middle_name", :name => "employee[middle_name]"
      assert_select "input#employee_last_name", :name => "employee[last_name]"
    end
  end
end
