require 'spec_helper'

describe "employees/index.html.erb" do
  before(:each) do
    assign(:employees, [
      stub_model(Employee,
        :first_name => "First Name",
        :middle_name => "Middle Name",
        :last_name => "Last Name"
      ),
      stub_model(Employee,
        :first_name => "First Name",
        :middle_name => "Middle Name",
        :last_name => "Last Name"
      )
    ])
  end

  it "renders a list of employees" do
    render
    # Run the generator again with the --webrat-matchers flag if you want to use webrat matchers
    assert_select "tr>td", :text => "First Name".to_s, :count => 2
    # Run the generator again with the --webrat-matchers flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Middle Name".to_s, :count => 2
    # Run the generator again with the --webrat-matchers flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Last Name".to_s, :count => 2
  end
end
