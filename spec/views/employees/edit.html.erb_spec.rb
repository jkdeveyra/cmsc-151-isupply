require 'spec_helper'

describe "employees/edit.html.erb" do
  before(:each) do
    @employee = assign(:employee, stub_model(Employee,
      :new_record? => false,
      :first_name => "MyString",
      :middle_name => "MyString",
      :last_name => "MyString"
    ))
  end

  it "renders the edit employee form" do
    render

    # Run the generator again with the --webrat-matchers flag if you want to use webrat matchers
    assert_select "form", :action => employee_path(@employee), :method => "post" do
      assert_select "input#employee_first_name", :name => "employee[first_name]"
      assert_select "input#employee_middle_name", :name => "employee[middle_name]"
      assert_select "input#employee_last_name", :name => "employee[last_name]"
    end
  end
end
