ISupply::Application.routes.draw do

  resources :purchase_requests do
    member do
      get 'suggestions'
    end
  end

  resources :app_masters do
    resources :apps
    member do
      get 'distribute'
      get 'deliver'
      get 'deliver_to_offices'
    end
  end

  devise_for :users, :skip => [:registrations, :sessions]

  as :user do
    get '/login' => 'devise/sessions#new', as: :new_user_session
    post '/login' => 'devise/sessions#create', :as => :user_session
    delete '/logout' => 'devise/sessions#destroy', as: :destroy_user_session
    get '/signup' => 'registration#new', as: :new_user_registration
    post '/signup' => 'registration#create', as: :new_user_registration
    get '/users/edit' => 'devise/registrations#edit', as: :edit_user_registration
    put '/users' => 'devise/registrations#update', as: :user_registration
  end

  resources :users

  post "/users/from_profile" => "users#create_from_profile"

  post "/users/:id/roles/remove/:role" => "users#remove_role"

  post "/users/:id/roles/add/:role" => "users#add_role"

  resources :phones do
    member do
      get 'delete'
      get 'add'
    end
  end

  resources :suppliers do
    member do
      get 'add_contact'
      get 'delete_contact'
    end
  end

  resources :offices do
    resources :app_offices do
      member do
        get 'submit'
        get 'print'
      end
    end
  end
  resources :profiles

  resources :employees

  resources :colleges

  resources :items

  resources :articles

  resources :article_categories

  root :to => "site#index"

  match "/home" => "site#home"

  match 'spso_home' => 'site#spso_home'

  match "/about" => "site#about"

end
