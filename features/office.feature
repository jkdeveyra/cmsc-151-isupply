Feature: Office

	As a SPSO Officer
	In order to establish organization in the college
	I want to create offices
	
	Scenario: Creating with the right information
		Given I am on "The Offices Page"
		When I follow "Create New Office"
		When I fill in "Name" with "Office of Student Affairs"
		And I select "Telephone" from "Type"
		And I fill in "Number" with "325-1234"
		And press "Submit"
		Then I should be able to add the office
	
	Scenario: Creating with blank information
		Given I am on "The Offices Page"
		When I follow "Create New Office"
		And press "Submit"
		Then I should not be able to add the office
		
	Scenario: Creating with lacking information
		Given I am on "The Offices Page"
		When I follow "Create New Office"
		When I fill in "Name" with "Office of Student Affairs"
		And I select "Telephone" from "Type"
		And press "Submit"
		Then I should be not able to add the office
	
	
		
